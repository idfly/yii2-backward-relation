# yii2-backward-relation

Модуль yii2 от idfly для оптимизации создания форм ввода данных один-ко-многим.


## Установка

Предпочтительный способ установки через [composer](http://getcomposer.org/download/).

В файл проекта `composer.json`, необходимо добавить следующий код:

```
"repositories": [
        {
            "type": "git",
            "url": "git@bitbucket.org:idfly/yii2-backward-relation.git"
        }
]
```

Затем запустить команду:

```
php composer.phar require --prefer-dist idfly/yii2-backward-relation "dev-master"
```

или добавить в разделе `require`, в файле вашего проекта `composer.json`, следующий код:

```
"idfly/yii2-backward-relation": "dev-master"
```

Подключить модуль в файле common.php:

```
$config['modules']['backwardRelation'] = ['class' => 'idfly\backwardRelation\Module'];
```


## Описание


### trait BackwardRelation


####Использование:

1. Добавить `use idfly\backwardRelation\components\BackwardRelation;` в файл
класса

2. Добавить `use BackwardRelation;` в класс для подключения trait'a

3. Определить свойство $_backwardRelations в классе, добавить туда
    информацию о связи в виде: `'KEY' => ['class' => 'CLASS', 'reference' => 'REFERENCE'];`

4. `KEY` - идентификатор обратной связи (например, 'phones' для телефонов
клиента)

5. `CLASS` - класс объектов, которые привязаны к данной модели (например,
'\app\models\ClientPhone')

6. `REFERENCE` - поле, по которому осуществляется привязка объектов

7. Добавить `$this->_validateBackwardRelations()` в `afterValidate`

8. Добавить `$this->_saveBackwardRelations()` в `afterSave`

9. Добавить `get` и `set` методы ключа

####Полный пример использования:

```
    class Client extends \yii\db\ActiveRecord {

        use \app\components\BackwardRelation;

        protected $_backwardRelations = [
            'phones' => [
                'class' => '\app\models\ClientPhone',
                'reference' => 'client_id',
            ],
        ];

        public function afterValidate() {
            if(!$this->_validateBackwardRelations()) {
                $this->addError('Не удалось сохранить внешние данные');
            };
        }

        public function afterSave($insert, $changedAttributes) {
            $this->_saveBackwardRelations();
            parent::afterSave($insert, $changedAttributes);
        }

        public function getPhones()
        {
            return $this->_getBackwardRelations('phones');
        }

        public function setPhones($phones)
        {
            $this->_setBackwardRelations('phones', $phones);
        }

    }
```

### class BackwardRelationHelper

####Описание:
Хелпер для создания форм ввода данных один-ко-многим.

####Использование:

```
    echo \app\components\BackwardRelationHelper::render(
        $client,
        'phones',
        function($phone, $index, $prefix) use($form) { ?>
            <div class="row margin-bottom phone-item">
                <input type="hidden" name="<?= Html::encode($prefix)
                    ?>[id]" value="<?= $phone->id ?>">

                <div class="col-sm-4">
                    <input type="text" class="form-control client-phone-phone" name="<?=
                        $prefix ?>[phone]" value="<?= Html::encode($phone->phone) ?>">
                    <?= $form->
                        field($phone, 'phone', ['template' => '{error}'])->
                        input('text') ?>
                </div>

                <div class="col-sm-1">
                    <button type="button" class="btn btn-danger backward-relation-delete">
                    <div class="fa fa-trash-o"></div></button>
                </div>
            </div>
            <?
        }
    );
```

### js-component IdFly.Components.BackwardRelation

####Описание:
Хелпер для создания форм ввода данных один-ко-многим.

####Использование:
Для использования данной функциональности форма должна содержать элементы
с классами `"backward-relation-add"` для добавления нового элемента и
`"backward-relation-delete"` для удаления существующего элемента.

####Пример использования:

```
IdFly.Components.BackwardRelation.init(element);
```