<?php

namespace idfly\backwardRelation;

use yii\web\AssetBundle;

class BackwardRelationAsset extends AssetBundle
{

    /** @inheritdoc */
    public $sourcePath = '@vendor/idfly/yii2-backward-relation/assets';

    /** @inheritdoc */
    public $css = [

    ];

    /** @inheritdoc */
    public $js = [
        'js/utility-backward-relation.js',
    ];

    /** @inheritdoc */
    public $depends = [
        'yii\web\YiiAsset',
    ];

    /** @inheritdoc */
    public $publishOptions = [
        'forceCopy' => false,
    ];
}
