var IdFly = IdFly || {};
IdFly.Components = IdFly.Components || {};

/**
 * Хелпер для создания форм ввода данных один-ко-многим.
 *
 * Для использования данной функциональности форма должна содержать элементы
 * с классами "backward-relation-add" для добавления нового элемента и
 * "backward-relation-delete" для удаления существующего элемента.
 *
 * Пример использования:
 *
 *   IdFly.Components.BackwardRelation.init(element);
 */
IdFly.Components.BackwardRelation = {

    init: function(element) {
        element = $(element);

        element.
            find('.backward-relation-row-new [name!=""]').
            each(function() {
                $(this).attr(
                    'data-backward-relation-name',
                    $(this).attr('name')
                );

                $(this).removeAttr('name');
            });

        element.
            find('.backward-relation-add').
            on('click', function() {
                var index = (element.data('backward-relation-index') || 0) + 1;
                element.data('backward-relation-index', index);

                var proto = element.find('.backward-relation-row-new');
                var newElement = proto.clone();

                newElement.
                    removeClass('backward-relation-row-new').
                    addClass('backward-relation-row').
                    show();

                newElement.
                    find('[data-backward-relation-name*="{new}"]').
                    each(function() {
                        var name =
                            $(this).attr('data-backward-relation-name').
                            replace('{new}', 'new.' + index);

                        $(this).
                            attr('name', name).
                            removeAttr('data-backward-relation-name');
                    });

                newElement.
                    find('[name$="[id]"]').
                    val('');

                proto.before(newElement);
            });

        element.
            find('.backward-relation-delete').
            unbind('click').
            on('click', function() {
                $(this).closest('.backward-relation-row').remove();
            });
    },
};