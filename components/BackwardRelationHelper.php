<?php

namespace idfly\backwardRelation\components;

/**
 * Хелпер для создания форм ввода данных один-ко-многим.
 *
 * Пример использования:
 *
 *   echo \app\components\BackwardRelationHelper::render(
 *       $client,
 *       'phones',
 *       function($phone, $index, $prefix) use($form) { ?>
 *           <div class="row margin-bottom phone-item">
 *               <input type="hidden" name="<?= Html::encode($prefix)
 *                   ?>[id]" value="<?= $phone->id ?>">
 *
 *               <div class="col-sm-4">
 *                   <input type="text" class="form-control client-phone-phone" name="<?=
 *                       $prefix ?>[phone]" value="<?= Html::encode($phone->phone) ?>">
 *                   <?= $form->
 *                       field($phone, 'phone', ['template' => '{error}'])->
 *                       input('text') ?>
 *               </div>
 *
 *               <div class="col-sm-1">
 *                   <button type="button" class="btn btn-danger backward-relation-delete"><div
 *                       class="fa fa-trash-o"></div></button>
 *               </div>
 *           </div>
 *           <?
 *       }
 *   );
 */
class BackwardRelationHelper
{

    public static function render($model, $type, $callback)
    {
        $result = '';
        foreach($model->{$type} as $index => $element) {
            $result .= self::_renderElement($model, $element, $type, $index,
                $callback, 'class="backward-relation-row"');
        }

        $class = $model->getBackwardRelationInfo($type)['class'];
        $element = new $class;
        $element->id = '{new}';

        $result .= self::_renderElement($model, $element, $type, 0, $callback,
            'class="backward-relation-row-new" style="display: none"');

        return $result;
    }

    protected static function _renderElement($model, $element, $type, $index,
        $callback, $attrs)
    {
        $elementHtml = '';

        ob_start();
        $id = $element->id;
        if($id !== '{new}') {
            if(empty($id)) {
                $id = 'ext.' . $index;
            } else {
                $id = 'old.' . $index;
            }
        }

        $prefix = $model->formName() . '[' . $type . '][' . $id . ']';
        $elementHtml .= (string)$callback($element, $index, $prefix);
        $elementHtml = ob_get_contents();
        ob_end_clean();

        return '<div ' . $attrs . '>' . $elementHtml . '</div>';
    }

}