<?php

namespace idfly\backwardRelation\components;

use yii\helpers\ArrayHelper;

/**
 * Использование:
 *
 *   1. Добавить use \app\components\BackwardRelation в класс
 *
 *   2. Определить свойство $_backwardRelations в классе, добавить туда
 *      информацию о связи в виде 'KEY' => ['class' => 'CLASS', 'reference' =>
 *      'REFERENCE'];
 *
 *   2.1. KEY - идентификатор обратной связи (например, 'phones' для телефонов клиента)
 *   2.2. CLASS - класс объектов, которые привязаны к данной модели (например, '\app\models\ClientPhone')
 *   2.3. REFERENCE - поле, по которому осуществляется привязка объектов
 *
 *   3. Добавить $this->_validateBackwardRelations() в afterValidate
 *
 *   4. Добавить $this->_saveBackwardRelations() в afterSave
 *
 *   5. Добавить get и set методы ключа
 *
 * Полный пример использования:
 *
 *   class Client extends \yii\db\ActiveRecord {
 *
 *       use \app\components\BackwardRelation;
 *
 *       protected $_backwardRelations = [
 *           'phones' => [
 *               'class' => '\app\models\ClientPhone',
 *               'reference' => 'client_id',
 *           ],
 *       ];
 *
 *       public function afterValidate() {
 *           if(!$this->_validateBackwardRelations()) {
 *               $this->addError('Не удалось сохранить внешние данные');
 *           };
 *       }
 *
 *       public function afterSave($insert, $changedAttributes) {
 *           $this->_saveBackwardRelations();
 *           parent::afterSave($insert, $changedAttributes);
 *       }
 *
 *       public function getPhones()
 *       {
 *           return $this->_getBackwardRelations('phones');
 *       }
 *
 *       public function setPhones($phones)
 *       {
 *           $this->_setBackwardRelations('phones', $phones);
 *       }
 *
 *   }
 *
 */

trait BackwardRelation {

    private $_backwardRelationsElements = [];

    public function getBackwardRelationInfo($type)
    {
        return $this->_backwardRelations[$type];
    }

    protected function _getBackwardRelations($type)
    {
        if(array_key_exists($type, $this->_backwardRelationsElements)) {
            return $this->_backwardRelationsElements[$type];
        }

        return $this->_hasMany($type);
    }

    protected function _setBackwardRelations($type, $elements)
    {
        $relation = $this->getBackwardRelationInfo($type);
        $class = $relation['class'];
        $this->_backwardRelationsElements[$type] = [];

        foreach($elements as $key => $element) {
            if(!is_array($element)) {
                $elementModel = $element;
            } else {
                if(empty($element['id'])) {
                    $elementModel = new $class();
                } else {
                    $elementModel =
                        $class::find()->
                        where(['id' => $element['id']])->
                        one();

                    if(empty($element)) {
                        throw new \Exception($type . ' not found');
                    }
                }

                $elementModel->setAttributes($element);
            }

            $this->_backwardRelationsElements[$type][] = $elementModel;
        }
    }

    protected function _validateBackwardRelations()
    {
        $success = true;
        foreach($this->_backwardRelationsElements as $type => $elements) {
            foreach($elements as $element) {
                $relation = $this->getBackwardRelationInfo($type);

                $attributes = array_diff($element->activeAttributes(),
                    [$relation['reference']]);

                if(!$element->validate($attributes)) {
                    $success = false;
                }
            }
        }

        return $success;
    }

    protected function _saveBackwardRelations()
    {
        $success = true;
        foreach($this->_backwardRelationsElements as $type => $elements) {
            $relation = $this->getBackwardRelationInfo($type);

            $oldElements = $this->_hasMany($type)->all();

            foreach($elements as $element) {
                $element->{$relation['reference']} = $this->id;
                $elementSuccess = $element->save();

                if(!$elementSuccess) {
                    $success = false;
                    $this->addError($type, 'не удалось сохранить данные ' .
                        '(' . $type . ')');
                }
            }

            $deleteElementsIds = array_diff(
                ArrayHelper::getColumn($oldElements, 'id'),
                ArrayHelper::getColumn($elements, 'id')
            );

            foreach($deleteElementsIds as $deleteElementId) {
                $class = $relation['class'];
                $model =
                    $class::find()->
                    where(['id' => $deleteElementId])->
                    one();

                if(empty($model)) {
                    throw new \Exception($type . ' not found');
                }

                if(!$model->delete()) {
                    $success = false;
                    $this->addError($type, 'не удалось сохранить данные ' .
                        '(' . $type . ')');
                }
            }
        }

        return $success;
    }

    private function _hasMany($type)
    {
        $relation = $this->getBackwardRelationInfo($type);
        return $this->hasMany($relation['class'], [
            $relation['reference'] => 'id'
        ]);
    }

}